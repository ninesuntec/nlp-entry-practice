import torch
from torch.utils.data import Dataset, DataLoader

data_path = r"C:\Users\NineSun\Desktop\archive\spam.txt"  # r表示后面是一个字符串，无需转义


# 完成数据集类
class MyDdataSet(Dataset):
    def __init__(self):
        self.lines = open(data_path, 'r', encoding='UTF-8').readlines()

    def __getitem__(self, index):
        # 获取索引对应位置的数据
        return self.lines[index]

    def __len__(self):
        # 返回数据的总数量
        return len(self.lines)


# if __name__ == '__main__':
#     my_dataset = MyDdataSet()
#     for i in range(len(my_dataset)):
#         print(i, my_dataset[i])
my_dataset = MyDdataSet()
# dataset:实例化之后的数据集；batch_size:batch的大小，一个batch包含10个样本数据；
# shuffle：表示是否打乱数据的顺序；num_workers：表示加载数据时启用线程的数量
data_loader = DataLoader(dataset=my_dataset, batch_size=2, shuffle=True, num_workers=2)

# 遍历，获取每个batch的结果
if __name__ == '__main__':
    for i in data_loader:
        print(i)
        break
    print(len(data_loader))
    print(len(my_dataset))
