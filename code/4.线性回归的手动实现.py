import torch
import numpy as np
from matplotlib import pyplot as plt

learning_rate = 0.01
# 1.准备数据 y=3x+0.8，准备参数
x = torch.rand([500, 1])  # 1阶，50行1列
y = 3 * x + 0.8
# 2.通过模型计算y_predict
w = torch.rand([1, 1], requires_grad=True)
b = torch.tensor(0, requires_grad=True, dtype=torch.float32)
y_predict = x * w + b

# 4.通过循环，反向传播，更新参数
for i in range(5000):  # 训练3000次
    # 计算预测值
    y_predict = x * w + b
    # 3.计算loss
    loss = (y_predict - y).pow(2).mean()
    if w.grad is not None:
        w.grad.data.zero_()
    if b.grad is not None:
        b.grad.data.zero_()
    loss.backward()  # 反向传播
    w.data = w.data - learning_rate * w.grad
    b.data = b.data - learning_rate * b.grad
    print("w:{},b:{},loss:{}".format(w.item(), b.item(), loss.item()))
plt.figure(figsize=(20, 8))
plt.scatter(x.numpy().reshape(-1), y.numpy().reshape(-1))  # 散点图
y_predict = x * w + b
# y_predict包含gard，所以我们需要深拷贝之后转numpy
plt.plot(x.numpy().reshape(-1), y_predict.detach().numpy().reshape(-1), color="red", linewidth=2, label="predict")  # 直线
plt.show()