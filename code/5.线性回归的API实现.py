import torch
import torch.nn as nn
from torch.optim import SGD
from matplotlib import pyplot as plt

# 1.定义数据
x = torch.rand([500, 1], dtype=torch.float32)  # 1阶，50行1列
y = 3 * x + 0.8


# 2.定义模型
class Lr(nn.Module):
    def __init__(self):
        super(Lr, self).__init__()  # 继承父类的init参数
        # linear=nn.Linear(input的特征数量，输出的特征数量)
        self.linear = nn.Linear(1, 1)  # 第一个参数：输入的形状 第二个参数：输出的形状 1：代表维度（也称为列数）
        # self.fcl = nn.Linear(1, 1)

    def forward(self, x):
        out = self.linear(x)
        # out = self.fcl(out)
        # out=nn.ReLU(out)
        return out


# 3.实例化模型，loss和优化器
model = Lr()
loss_fn = nn.MSELoss()
optimizer = SGD(model.parameters(), 0.001)
# 4.训练模型
for i in range(30000):
    y_predict = model(x)  # 获取预测值
    loss = loss_fn(y, y_predict)  # 计算损失
    optimizer.zero_grad()  # 参数梯度置0
    loss.backward()  # 回归计算梯度
    optimizer.step()  # 更新梯度
    print("损失：{}".format(loss.data))
# 5.模型评估
model.eval()  # 设置模型为评估模式，即预测模式
predict = model(x)
predict = predict.data.numpy()
plt.scatter(x.data.numpy(), y.data.numpy(), c='b')
plt.plot(x.data.numpy(), predict, c='r')
plt.show()
