import torch

x = torch.ones(2, 2, requires_grad=True)  # 初始化参数x，并设置requires_grad=True用于追踪其计算历史
print("x=", x)
y = x + 2
print("y=", y)
z = y * y * 3  # 平方*3
print("z=", z)
out = z.mean()  # 求均值
print("out=", out)
a = torch.randn(2, 2)
a = ((a * 3) / (a - 1))
print(a.requires_grad)
a.requires_grad_(True)  # 就地修改
print(a.requires_grad)
b = (a * a).sum()
print(b.requires_grad)
with torch.no_grad():
    c = (a * a).sum()
print(c.requires_grad)
out.backward()  # 反向传播
print("反向传播：", x.grad)  # x.grad获取梯度
print(a)
print(a.data)

