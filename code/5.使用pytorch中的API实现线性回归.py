from torch import nn, optim
import torch


class Lr(nn.Module):
    def __init__(self):
        super(Lr, self).__init__()  # 继承父类的init参数
        self.linear = nn.Linear(1, 1)  # 第一个参数：输入的形状 第二个参数：输出的形状 1：代表维度（也称为列数）

    def forward(self, x):
        out = self.linear(x)
        return out


# 实例化模型
model = Lr()
# 传入数据，计算结果
x = torch.rand([500, 1])  # 1阶，50行1列
predict = model(x)
y = 3 * x + 0.8
model = Lr()  # 1.实例化模型
criterion = nn.MSELoss()  # 2.实例化损失函数
optimizer = optim.SGD(model.parameters(), lr=x.le - 3)  # 3.实例化优化器
for i in range(100):
    y_predict = model(x)  # 4.向前传播
    loss = criterion(y, y_predict)  # 5.调用损失函数传入真实值和预测值，得到损失结果
    optimizer.zero_grad()  # 5.当前循环参数梯度置为0
    loss.backward()  # 6.计算梯度
    optimizer.step()  # 7.更新参数的值
